## uniapp-extend

#### 介绍
##### uni-app 组件 / 模板 / SDK 分享

### 在线预览：[https://mydarling.gitee.io/uniapp-extend](https://mydarling.gitee.io/uniapp-extend "H5版在线体验")

---

#### 功能清单

##### 通用组件
* 颜色选择器
* 拖动滑块验证
* 异步switch
* 指定内容高亮

##### 页面模板
* 看图猜成语
* 有入场动画的图片列表
* 仿微信列表长按弹窗菜单
* 商品双向联动列表

##### 通用SDK
* 项目全局样式表
* 项目全局方法封装
* webSocket项目SDK

---

#### 安装
* 下载项目 导入 HBuilderX 即可运行体验

#### 关于作者
* 昵称：河浪
* 简介：一个热爱分享又普通的web前端开发者
* 邮箱：helang.love@qq.com

#### 资源分享
1. uni-app插件主页：[https://ext.dcloud.net.cn/publisher?id=110853](https://ext.dcloud.net.cn/publisher?id=110853 "uni-app插件主页")
2. jQuery插件主页：[http://www.jq22.com/mem395541](http://www.jq22.com/mem395541 "jQuery插件主页：")
3. CSDN-博客主页：[https://blog.csdn.net/u013350495](https://blog.csdn.net/u013350495 "CSDN-博客主页")



